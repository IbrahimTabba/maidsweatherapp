import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceHelper {

  SharedPreferences sp;
  static SharedPreferenceHelper _instance;
  static SharedPreferenceHelper get instance => _instance;

  factory SharedPreferenceHelper({@required SharedPreferences sp}){
    _instance??=SharedPreferenceHelper._(sp: sp);
    return _instance;
  }

  SharedPreferenceHelper._({@required this.sp});

  static Future<void> addStringToSF(String key, String value)async{
    await _instance.sp.setString(key, value);
  }
  static Future<void> addBooleanToSF(String key, bool value) async {
    await _instance.sp.setBool(key, value);
  }
  static Future<void> addIntToSF(String key, int value) async {
    await _instance.sp.setInt(key, value);
  }
  static String getStringFromSP(String key , {String init = ''}){
    return _instance.sp.containsKey(key)?_instance.sp.getString(key):init;
  }
  static bool getBooleanFromSP(String key , {bool init = false}){
    return _instance.sp.containsKey(key)?_instance.sp.getBool(key):init;
  }
  static int getIntFromSP(String key , {int init = 0})  {
    return _instance.sp.containsKey(key)?_instance.sp.getInt(key):init;
  }
}
