extension StringUtils on String{
  String addParams(Map<String,dynamic> obj) {
    var res = this;
    var initial =true;
    obj.forEach((key, value) {
      res+='${initial?'?':'&'}$key=$value';
      initial = false;
    });
    return res;
  }

  String get capitalizeFirstLitterMinimizeOthers {
    if (isEmpty) {
      return this;
    } else if (length == 1) {
      return toUpperCase();
    } else {
      return '${this[0].toUpperCase()}${substring(1).toLowerCase()}';
    }
  }
}

extension prettyDouble on double{
  String prettify(fractionDigits){
    var formatted = this.toStringAsFixed(fractionDigits);
    if(formatted.contains(RegExp(r'\.0+')))
      formatted = formatted.replaceAll(RegExp(r'\.0+'),"");
    if(formatted.contains(RegExp(r'\.\d\d')))
      formatted = formatted.replaceRange(formatted.length-1, formatted.length, "");
    return formatted;
  }
}