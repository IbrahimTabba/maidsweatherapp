import 'package:maidsweatherapp/helpers/shared_preference_helper.dart';

class WeatherForecastManager{
  static String getStoredResponse(DateTime date){
    var key = "${date.year}-${date.month}-${date.day}";
    var lastResponse = SharedPreferenceHelper.getStringFromSP('last_forecast_response_date',init: '');
    if(key == lastResponse)
      return (SharedPreferenceHelper.getStringFromSP('last_forecast_response',init: null));
    return null;
  }

  static Future<bool> storeResponse(String response)async{
    var date = DateTime.now();
    var key = "${date.year}-${date.month}-${date.day}";

    await SharedPreferenceHelper.addStringToSF('last_forecast_response_date', key);
    await SharedPreferenceHelper.addStringToSF('last_forecast_response', response);
    return true;

  }

  static clear()async{
    await SharedPreferenceHelper.instance.sp.remove('last_forecast_response_date');
    await SharedPreferenceHelper.instance.sp.remove('last_forecast_response');
    return true;
  }
}