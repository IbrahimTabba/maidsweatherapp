import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:maidsweatherapp/helpers/shared_preference_helper.dart';
import 'package:maidsweatherapp/resources/repositories/weather/wather_forecast_repository.dart';
import 'package:maidsweatherapp/src/app.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  WeatherForecastRepository(Client());
  SharedPreferenceHelper(sp: await SharedPreferences.getInstance());
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
    statusBarColor: Colors.transparent,
    statusBarIconBrightness: Brightness.light
  ));
  runApp(App());
}
