import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maidsweatherapp/ui/screens/day_pick_screen.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
     home: DayPickScreen()
    );
  }
}
