import 'package:flutter/material.dart';
import 'package:http/http.dart';

abstract class BaseRepository {
  final Client client;
  BaseRepository({@required this.client});
}