import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:maidsweatherapp/models/weather_forecast_response_model.dart';
import 'package:maidsweatherapp/resources/providers/weather/weather_forecast_provider.dart';
import 'package:maidsweatherapp/resources/repositories/base_repository.dart';

class WeatherForecastRepository extends BaseRepository{

  static WeatherForecastRepository _instance;
  final WeatherForeCastProvider _weatherForeCastProvider = WeatherForeCastProvider();

  WeatherForecastRepository._(Client client):super(client : client);

  factory WeatherForecastRepository(Client client){
    _instance??=WeatherForecastRepository._(client);
    return _instance;
  }

  static Future<WeatherForeCastResponseModel> getWeatherForecastByCityId({@required cityId , dayCount , units , @required appKey})async{
    return await _instance._weatherForeCastProvider.getWeatherForecastByCityId(client: _instance.client, cityId: cityId, appKey: appKey,units: units,dayCount: dayCount);
  }

}