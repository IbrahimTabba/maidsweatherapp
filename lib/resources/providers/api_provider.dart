abstract class ApiProvider{

  String getEndPointUrl();
  dynamic get headers async => {
    'Content-Type':'application/json',
    'Accept':'application/json'
  };
}