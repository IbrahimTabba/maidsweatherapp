import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:maidsweatherapp/managers/weather_forecast_manager.dart';
import 'package:maidsweatherapp/models/weather_forecast_response_model.dart';
import 'package:maidsweatherapp/resources/providers/api_provider.dart';
import 'package:maidsweatherapp/extentions/extentions.dart';

class WeatherForeCastProvider extends ApiProvider{
  @override
  String getEndPointUrl()=>'https://api.openweathermap.org/data/2.5/forecast';

  Future<WeatherForeCastResponseModel> getWeatherForecastByCityId({@required Client client , @required cityId , dayCount , units , @required appKey})async{
    client??=Client();
    try{
      var cachedResponse = WeatherForecastManager.getStoredResponse(DateTime.now());
      if(cachedResponse==null)
        await WeatherForecastManager.clear();

      if(cachedResponse!=null)
        return WeatherForeCastResponseModel.fromJson(json.decode(cachedResponse));

      var response = await client.get(getEndPointUrl().addParams({
        'id':cityId,
        'appid':appKey,
        if(dayCount!=null)
          'cnt':dayCount,
        'units':units,
      }),headers: await headers);

      if(response.statusCode!=200)
        return WeatherForeCastResponseModel.withError(code: response.statusCode);

      await WeatherForecastManager.storeResponse(utf8.decode(response.bodyBytes));

      return WeatherForeCastResponseModel.fromJson(json.decode(utf8.decode(response.bodyBytes)));

    }catch(e){
      return WeatherForeCastResponseModel.withError();
    }
  }

}