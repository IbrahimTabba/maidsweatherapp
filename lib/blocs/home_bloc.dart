import 'package:flutter/cupertino.dart';
import 'package:maidsweatherapp/models/weather_forecast_response_model.dart';
import 'package:maidsweatherapp/resources/repositories/weather/wather_forecast_repository.dart';
import 'package:rxdart/rxdart.dart';
import 'multi_stream_bloc.dart';

class WeatherForeCastBloc extends MultiStreamBaseBloc {
  WeatherForeCastBloc(List<Subject> subjects) : super(subjects);

  BehaviorSubject<WeatherForeCastResponseModel> get forecast => subjectAt(0);

  getWeatherForecastByCityId({@required cityId , dayCount , units = 'metric' , @required appKey , retry = false})async{
    if(retry)
      forecast.sink.add(null);
    var response = await WeatherForecastRepository.getWeatherForecastByCityId(cityId: cityId, appKey: appKey,units: units,dayCount: dayCount);
    forecast.sink.add(response);
  }


}

final weatherForeCastBloc = WeatherForeCastBloc([
  BehaviorSubject<WeatherForeCastResponseModel>(),
]);
