import 'package:rxdart/rxdart.dart';


abstract class MultiStreamBaseBloc {
  final List<Subject> _subjects;

  List<Subject> get subject => _subjects;

  MultiStreamBaseBloc(this._subjects);

  Subject subjectAt(idx)=>_subjects[idx];

  void dispose() {
    _subjects.forEach((subject) {subject.close();});
  }
}