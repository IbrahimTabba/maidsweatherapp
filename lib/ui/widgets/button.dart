import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
class Button extends StatefulWidget {

  final String text;
  final VoidCallback onPressed;
  final Color color;
  final Color backgroundColor;
  final double verticalPadding;
  final double horizontalPadding;
  final bool border;
  final double borderRadius;
  final Color tapBackgroundColor;
  final Color tapTextColor;
  final Widget icon;
  final bool hasIcon;
  final double fontSize;
  final Color actionColor;
  final matchParent;


  Button(this.text, {this.onPressed, this.actionColor , this.fontSize = 18.0, this.color, this.backgroundColor, this.verticalPadding = 0.0,this.horizontalPadding = 16.0, this.borderRadius = 30.0, this.border : false, this.tapBackgroundColor, this.tapTextColor,this.hasIcon = false, this.icon , this.matchParent = true }) ;

  @override
  _ButtonState createState() => _ButtonState();
}

class _ButtonState extends State<Button> {
  bool _isTapped = false;
  GlobalKey buttonKey;
  double buttonHeight;
  bool sizeCalculated = false;
  @override
  void initState() {
    sizeCalculated = false;
    buttonKey = GlobalKey();
    super.initState();
  }

  _rendered(BuildContext context){
    if(!sizeCalculated && buttonKey!=null && buttonKey.currentContext!=null){
      var sliderRenderBox = buttonKey.currentContext.findRenderObject() as RenderBox;
      setState(() {
        buttonHeight = sliderRenderBox.size.height;
        sizeCalculated = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_)=>{_rendered(context)});
    var button =  FlatButton(
      shape: widget.border ? RoundedRectangleBorder(side: BorderSide(
          color: _isTapped ? widget.tapTextColor : widget.color,
          width: 1,
          style: BorderStyle.solid
      ), borderRadius: BorderRadius.circular(this.widget.borderRadius)) : RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(this.widget.borderRadius)),
      onPressed: widget.onPressed,
      hoverColor: widget.color.withOpacity(0.25),
      onHighlightChanged: (d) {
        setState(() {
          _isTapped = !_isTapped;
        });
      },
      padding: EdgeInsets.symmetric(vertical: widget.verticalPadding , horizontal: widget.horizontalPadding),
      highlightColor: Colors.transparent,
      color: (_isTapped && widget.tapBackgroundColor != null) ? widget.tapBackgroundColor : widget.backgroundColor,
      child: widget.hasIcon ? widget.icon:
      AutoSizeText(
        widget.text,
        maxLines: 1,
        style: TextStyle(
            color:  (_isTapped && widget.tapTextColor != null) ? widget.tapTextColor : widget.color,
            fontWeight: FontWeight.w400,
            fontSize: widget.fontSize
        ),
      ),
    );
    if(!widget.matchParent)
      return button;
    return SizedBox(
      width: widget.matchParent?double.infinity:null,
      key: buttonKey,
      child: button,
    );
  }
}
