import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maidsweatherapp/ui/colors.dart';
import 'package:maidsweatherapp/ui/widgets/button.dart';

class NoResponse extends StatelessWidget {
  final retryCallBack;

  const NoResponse({Key key, this.retryCallBack}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Couldn't get any response please try again",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 22,
              color: Colors.black.withOpacity(0.5)
            ),
          ),
          SizedBox(height: 16,),
          Button(
              'Retry',
              matchParent: false,
              color: Colors.white,
              fontSize: 14,
              verticalPadding: 4,
              backgroundColor: accentColor,
              actionColor: accentColor,
              tapTextColor: Colors.white,
              tapBackgroundColor: accentColor.withOpacity(0.7),
              onPressed: (){
                if(retryCallBack!=null)
                  retryCallBack();
              },
            )
        ],
      ),
    );
  }
}
