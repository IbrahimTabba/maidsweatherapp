import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maidsweatherapp/ui/colors.dart';

class WeatherPropertyBox extends StatelessWidget {
  final title;
  final value;

  const WeatherPropertyBox({Key key, this.title, this.value}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 70,
      padding: EdgeInsets.symmetric(horizontal: 12,vertical: 12),
      decoration: BoxDecoration(
        color: lightTextColor,
        borderRadius: BorderRadius.circular(4)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if(title!=null)
            AutoSizeText(
              title,
              maxLines: 1,
              style: TextStyle(
                  color: Colors.grey
              ),
            ),
          SizedBox(height: 4,),
          if(value!=null)
            Row(
              children: [
                Expanded(
                    child: AutoSizeText(
                      value,
                      minFontSize: 0,
                      stepGranularity: 0.1,
                      maxLines: 1,
                      style: TextStyle(
                        color: Colors.black.withOpacity(0.7),
                        fontSize: 18
                      ),
                    )
                )
              ],
            )
        ],
      ),
    );
  }
}
