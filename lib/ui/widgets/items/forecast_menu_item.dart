import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animator/flutter_animator.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:maidsweatherapp/models/city_model.dart';
import 'package:maidsweatherapp/models/day_weather_forecast_model.dart';
import 'package:maidsweatherapp/extentions/extentions.dart';
import 'package:maidsweatherapp/ui/screens/forecast_details_screen.dart';

class ForecastMenuItem extends StatelessWidget {
  final DayWeatherForeCastModel forecast;
  final CityModel city;
  final MenuForecastItemMode mode;
  final loading;

  const ForecastMenuItem({Key key, this.forecast, this.city, this.mode = MenuForecastItemMode.Collapsed , this.loading = false}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()async{
        Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>ForecastDetailsScreen(city: city,forecast: forecast,)));
      },
      child: mode == MenuForecastItemMode.Collapsed? _collapsedCard(context):_expandedCard(context),
    );
  }

  _collapsedCard(BuildContext context){
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Color(0xff38A7C2) , Color(0xff38A7C2).withOpacity(0.7)]
        ),
        borderRadius: BorderRadius.circular(14),
        image: DecorationImage(
          image: AssetImage(city.day?'assets/images/clear_sky_day.jpg':'assets/images/night_sky.jpg'),
          fit: BoxFit.cover,
          colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
        )
      ),
      margin: EdgeInsets.only(bottom: 8),
      child: Stack(
        children: [
          Container(
            //width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(horizontal: 16 , vertical: 6),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 2,
                  child: AutoSizeText(
                      DateFormat('EEEE').format(forecast.foreCastedDate).toUpperCase(),
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w700
                      ),
                  ),
                ),
                SizedBox(width: 8,),
                Expanded(
                  flex: 1,
                  child: AutoSizeText(
                      "${forecast.temperature.prettify(0)}°",
                      style: TextStyle(
                          fontSize: 22,
                          color: Colors.white,
                          fontWeight: FontWeight.w700
                      ),
                  ),
                ),
                SizedBox(width: 8,),
                Expanded(
                  flex: 1,
                  child: Container(
                      height: 64,
                      padding: EdgeInsets.symmetric(vertical: 8),
                      child: SvgPicture.asset(forecast.weather.first.svgAssetPath),
                      //child: Image.network(forecast.weather.first.icon)
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _expandedCard(BuildContext context){
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [Color(0xff38A7C2) , Color(0xff38A7C2).withOpacity(0.7)]
          ),
          borderRadius: BorderRadius.circular(14),
          image: DecorationImage(
            image: AssetImage(city.day?'assets/images/clear_sky_day.jpg':'assets/images/night_sky.jpg'),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
          )
      ),
      margin: EdgeInsets.only(bottom: 8),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(topLeft: Radius.circular(14) , topRight: Radius.circular(14)),
              color: Color(0xff16282F),
            ),
            padding: EdgeInsets.symmetric(horizontal: 16 , vertical: 12),
            child: Row(
              children: [
                Expanded(child: Text(
                  city.name,
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w700
                  ),
                ))
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AutoSizeText(
                        DateFormat('EEEE').format(forecast.foreCastedDate).toUpperCase(),
                        style: TextStyle(
                            fontSize: 24,
                            color: Colors.white,
                            fontWeight: FontWeight.w700
                        ),
                      ),
                      SizedBox(height: 22,),
                      AutoSizeText(
                        "${forecast.temperature.prettify(0)}°C",
                        style: TextStyle(
                            fontSize: 30,
                            color: Colors.white,
                            fontWeight: FontWeight.w700
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 8,),
                Expanded(
                  flex: 2,
                  child: Pulse(
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 16),
                      child: SvgPicture.asset(forecast.weather.first.svgAssetPath),
                      //child: Image.network(forecast.weather.first.icon)
                    ),
                    preferences: AnimationPreferences(
                        autoPlay: AnimationPlayStates.Loop,
                        duration: Duration(seconds: 3),
                        offset: Duration(milliseconds: 300)
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 8,),
          Row(
            children: [
              Expanded(child: Container(
                margin: EdgeInsets.symmetric(horizontal: 16),
                height: 1,color: Colors.white,
              )),
            ],
          ),
          SizedBox(height: 8,),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: AutoSizeText(
                    DateFormat("d MMMM").format(forecast.foreCastedDate).toUpperCase(),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16
                    ),
                  ),
                ),
                SizedBox(width: 8,),
                Expanded(
                  flex: 1,
                  child: AutoSizeText(
                    forecast.weather.first.weatherStatusDescription.capitalizeFirstLitterMinimizeOthers,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16
                    ),
                  )
                ),
              ],
            ),),
          SizedBox(height: 8,),
          Row(
            children: [
              Expanded(child: Container(
                margin: EdgeInsets.symmetric(horizontal: 16),
                height: 1,color: Colors.white,
              )),
            ],
          ),
          SizedBox(height: 8,),

        ],
      ),
    );
  }
}

enum MenuForecastItemMode{
  Expanded , Collapsed
}
