import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maidsweatherapp/models/city_model.dart';
import 'package:maidsweatherapp/models/day_weather_forecast_model.dart';
import 'package:maidsweatherapp/ui/colors.dart';
import 'package:maidsweatherapp/ui/widgets/items/wather_property_box.dart';
import 'package:maidsweatherapp/extentions/extentions.dart';


class ForeCastDetails extends StatelessWidget {
  final DayWeatherForeCastModel forecast;
  final CityModel city;

  const ForeCastDetails({Key key, this.forecast, this.city }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        physics: BouncingScrollPhysics(),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            SizedBox(width: 32,),
            Container(
              width: MediaQuery.of(context).size.width/2 - 48 ,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  if(city!=null)
                    AutoSizeText(
                      city.name,
                      style: TextStyle(
                        color: lightTextColor,
                        fontSize: 32
                      ),
                    ),
                  SizedBox(height: 4,),
                  if(forecast!=null)
                    AutoSizeText(
                      "${forecast.temperature.prettify(0)}°",
                      maxLines: 1,
                      style: TextStyle(
                        color: lightTextColor,
                        fontSize: 72,
                        fontWeight: FontWeight.w400
                      ),
                    )
                ],
              ),
            ),
            SizedBox(width: 16,),
            Padding(
              padding: const EdgeInsets.only(bottom: 8),
              child: WeatherPropertyBox(
                title: 'Wind',
                //Meter per second to Kilometer per hour
                value: "${(forecast.windSpeed*3.6).prettify(2)} Km/h",
              ),
            ),
            SizedBox(width: 8,),
            Padding(
              padding: const EdgeInsets.only(bottom: 8),
              child: WeatherPropertyBox(
                title: 'Humidity',
                value: "${forecast.humidity.prettify(2)}%",
              ),
            ),
            SizedBox(width: 32,),
          ],
        ),
      ),
    );
  }
}
