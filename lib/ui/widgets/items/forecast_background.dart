import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:maidsweatherapp/models/city_model.dart';
import 'package:maidsweatherapp/models/day_weather_forecast_model.dart';

class ForecastBackground extends StatelessWidget {
  final DayWeatherForeCastModel forecast;
  final CityModel city;

  const ForecastBackground({Key key, this.forecast , this.city}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = (MediaQuery.of(context).size.width/4)*3;
    return Center(
      child: Stack(
        children: [
          Center(
            child: Container(
              width: width,
              padding: EdgeInsets.symmetric(),
              child: SvgPicture.asset(
                forecast.weather.first.svgAssetPath,
                width: width*0.8,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
