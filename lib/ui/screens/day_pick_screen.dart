import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:maidsweatherapp/blocs/home_bloc.dart';
import 'package:maidsweatherapp/models/weather_forecast_response_model.dart';
import 'package:maidsweatherapp/ui/widgets/items/forecast_menu_item.dart';
import 'package:maidsweatherapp/ui/widgets/no_response.dart';
import 'package:maidsweatherapp/ui/widgets/stateful_wrapper.dart';
import 'package:simple_animations/simple_animations.dart';

class DayPickScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.light
    ));
    return Scaffold(
      appBar: AppBar(
        elevation: 4,
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        title: Text('Weather Forecast' , style: TextStyle(color: Colors.black),),
      ),
      backgroundColor: Color(0xffE2E2E2),
      body: SafeArea(
        child: StatefulWrapper(
            onInit: (){
              _getData();
            },
            child: StreamBuilder(
              stream: weatherForeCastBloc.forecast.stream,
              builder: (context,AsyncSnapshot<WeatherForeCastResponseModel> snapshot){
                if(snapshot.hasData && !snapshot.data.error){
                  return _getBody(snapshot.data);
                }
                else if(snapshot.hasData && snapshot.data.error)
                  return NoResponse(retryCallBack: ()=>_getData(retry: true),);
                return Center(
                  child: SpinKitCircle(
                    color: Color(0xff273D64),
                    size: 50,
                  ),
                );
              },
            )
        ),
      ),
    );
  }

  _getBody(WeatherForeCastResponseModel weatherForeCastResponse){
    var data = weatherForeCastResponse.getByDate(DateTime.now().toUtc());
    return PlayAnimation(
      tween: Tween<double>(begin: 0.0 , end: 1.0),
      builder: (context,child,value){
        return Opacity(opacity: value,child: child,);
      },
      child: RefreshIndicator(
        onRefresh: () async {
          await _getData();
          return true;
        },
        color: Color(0xff273D64),
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 16 , vertical: 16),
          children: data.map(
                  (e) =>
                  ForecastMenuItem(
                    city: weatherForeCastResponse.city,
                    forecast: e,
                    mode: data.indexOf(e)==0?MenuForecastItemMode.Expanded:MenuForecastItemMode.Collapsed,
                  )).toList(),
        ),
      ),
      duration: Duration(milliseconds: 750),
    );
  }

  _getData({retry = false})async{
    await weatherForeCastBloc.getWeatherForecastByCityId(cityId: 292223, appKey: "4f9173e61ffe1d97903baafddaa3d6e1",retry: retry);
  }
}
