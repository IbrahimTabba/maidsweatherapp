import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_animator/flutter_animator.dart';
import 'package:maidsweatherapp/models/city_model.dart';
import 'package:maidsweatherapp/models/day_weather_forecast_model.dart';
import 'package:maidsweatherapp/ui/colors.dart';
import 'package:maidsweatherapp/ui/widgets/items/forecast_background.dart';
import 'package:maidsweatherapp/ui/widgets/items/forecast_details.dart';
import 'package:simple_animations/simple_animations.dart';

class ForecastDetailsScreen extends StatelessWidget {
  final DayWeatherForeCastModel forecast;
  final CityModel city;

  const ForecastDetailsScreen({Key key, this.forecast, this.city}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: null,//AppBar(toolbarHeight: 0, brightness: Brightness.dark,),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Container(
          decoration: BoxDecoration(
              gradient:LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [forecast.getColor(city) , forecast.getColor(city).withOpacity(0.8) , forecast.getColor(city).withOpacity(0.3)]
              )
          ),
          child: SafeArea(
            child: Stack(
              children: [
                PlayAnimation(
                  tween: Tween<double>(begin: 0.0 , end: 1.0),
                    builder: (context,child,value){
                      return Opacity(
                          opacity: value,
                          child: child,
                      );
                    },
                    child: Pulse(
                        child: ForecastBackground(forecast: forecast,city: city,),
                        preferences: AnimationPreferences(
                          autoPlay: AnimationPlayStates.Loop,
                          duration: Duration(seconds: 3),
                          offset: Duration(milliseconds: 300)
                        ),
                    ),
                    duration: Duration(milliseconds: 750),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsetsDirectional.only(start: 32 , top: 32),
                    child: Text(
                      forecast.weather.first.weatherStatusDescription.replaceAll(' ', '\n'),
                      style: TextStyle(
                        color: lightTextColor,
                        fontSize: 35
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: PlayAnimation<double>(
                    tween: Tween<double>(begin: 250 , end: 0),
                    builder: (context, child, value){
                      return Transform.translate(
                          offset: Offset(0,value),
                          child: child,
                      );
                    },
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ForeCastDetails(forecast: forecast,city: city,),
                        SizedBox(height: 18,)
                      ],
                    ),
                    duration: Duration(milliseconds: 300),
                    delay: Duration(milliseconds: 150),
                    curve: Curves.easeInOut,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
