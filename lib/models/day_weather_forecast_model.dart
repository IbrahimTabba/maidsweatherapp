import 'package:flutter/material.dart';
import 'package:maidsweatherapp/models/city_model.dart';

class DayWeatherForeCastModel{

  final DateTime foreCastedDate;
  final double minDailyTemperature;
  final double temperature;
  final double maxDailyTemperature;
  final double humidity;
  final double windSpeed;
  final List<WeatherStatusModel> weather;

  DayWeatherForeCastModel({this.foreCastedDate, this.minDailyTemperature, this.maxDailyTemperature, this.temperature, this.humidity, this.windSpeed, this.weather});


  DayWeatherForeCastModel.fromJson(obj):
        foreCastedDate = DateTime.fromMillisecondsSinceEpoch(obj['dt']*1000),
        temperature = (obj['main']['temp'] as num).toDouble(),
        minDailyTemperature = (obj['main']['temp_min'] as num).toDouble(),
        maxDailyTemperature = (obj['main']['temp_max'] as num).toDouble(),
        humidity = (obj['main']['humidity'] as num).toDouble(),
        windSpeed = (obj['wind']['speed'] as num).toDouble(),
        weather = obj['weather']!=null?(obj['weather'] as List).map((e) => WeatherStatusModel.fromJson(e)).toList():null;

  Color getColor(CityModel city){
    if(city.day){
      //Day Case
      switch(weather.first.status){
        case WeatherStatusEum.Thunderstorm:
          return Color(0xff3C4B4D);
          break;
        case WeatherStatusEum.Drizzle:
          return Color(0xff3C4B4D);
          break;
        case WeatherStatusEum.Rain:
          return Color(0xff83B7B5);
          break;
        case WeatherStatusEum.Snow:
          return Color(0xff83B7B5);
          break;
        case WeatherStatusEum.Mist:
          return Color(0xff83B7B5);
          break;
        case WeatherStatusEum.Smoke:
          return Color(0xff83B7B5);
          break;
        case WeatherStatusEum.Haze:
          return Color(0xff83B7B5);
          break;
        case WeatherStatusEum.Dust:
          return Color(0xffF7B994);
          break;
        case WeatherStatusEum.Fog:
          return Color(0xff83B7B5);
          break;
        case WeatherStatusEum.Sand:
          return Color(0xffF7B994);
          break;
        case WeatherStatusEum.Ash:
          return Color(0xffF7B994);
          break;
        case WeatherStatusEum.Squall:
          return Color(0xffF7B994);
          break;
        case WeatherStatusEum.Tornado:
          return Color(0xff3C4B4D);
          break;
        case WeatherStatusEum.Clear:
          return Color(0xffF7B994);
          break;
        case WeatherStatusEum.Clouds:
          return Color(0xff83B7B5);
          break;
        case WeatherStatusEum.UnKnown:
          return Color(0xffF7B994);
          break;
      }
    }
    else{
      //Night case
      return Colors.grey;
    }
  }

}

class WeatherStatusModel{
  final WeatherStatusEum status;
  final String weatherStatusDescription;
  final String icon;
  String svgAssetPath;

  WeatherStatusModel(this.status, this.weatherStatusDescription , this.icon);

  WeatherStatusModel.fromJson(obj):
        status = _weatherStatusByGroup(obj['main']),
        weatherStatusDescription = obj['description'],
        icon = "http://openweathermap.org/img/wn/${obj['icon']}@2x.png"{
    _getSvgAsset();
  }

  static _weatherStatusByGroup(String group){
    var groupMap = {
      "Thunderstorm":WeatherStatusEum.Thunderstorm,
      "Drizzle":WeatherStatusEum.Drizzle,
      "Rain":WeatherStatusEum.Rain,
      "Snow":WeatherStatusEum.Snow,
      "Mist":WeatherStatusEum.Mist,
      "Smoke":WeatherStatusEum.Smoke,
      "Haze":WeatherStatusEum.Haze,
      "Dust":WeatherStatusEum.Dust,
      "Fog":WeatherStatusEum.Fog,
      "Sand":WeatherStatusEum.Sand,
      "Ash":WeatherStatusEum.Ash,
      "Squall":WeatherStatusEum.Squall,
      "Tornado":WeatherStatusEum.Tornado,
      "Clear":WeatherStatusEum.Clear,
      "Clouds":WeatherStatusEum.Clouds,
    };
    return groupMap[group]??WeatherStatusEum.UnKnown;
  }

  _getSvgAsset(){
    svgAssetPath = 'assets/svgs/weather_pack_1/';
    switch(status){
      case WeatherStatusEum.Thunderstorm:
        svgAssetPath+='thunderstorm.svg';
        break;
      case WeatherStatusEum.Drizzle:
        svgAssetPath+='shower_rain.svg';
        break;
      case WeatherStatusEum.Rain:
        if(weatherStatusDescription.toLowerCase() == 'freezing rain'.toLowerCase())
          svgAssetPath+='snow.svg';
        else
          svgAssetPath+='rain.svg';
        break;
      case WeatherStatusEum.Snow:
        svgAssetPath+='snow.svg';
        break;
      case WeatherStatusEum.Mist:
        svgAssetPath+='mist.svg';
        break;
      case WeatherStatusEum.Smoke:
        svgAssetPath+='mist.svg';
        break;
      case WeatherStatusEum.Haze:
        svgAssetPath+='mist.svg';
        break;
      case WeatherStatusEum.Dust:
        svgAssetPath+='mist.svg';
        break;
      case WeatherStatusEum.Fog:
        svgAssetPath+='mist.svg';
        break;
      case WeatherStatusEum.Sand:
        svgAssetPath+='mist.svg';
        break;
      case WeatherStatusEum.Ash:
        svgAssetPath+='mist.svg';
        break;
      case WeatherStatusEum.Squall:
        svgAssetPath+='mist.svg';
        break;
      case WeatherStatusEum.Tornado:
        svgAssetPath+='mist.svg';
        break;
      case WeatherStatusEum.Clear:
        if(icon.contains('d'))
          svgAssetPath+='clear_sky_day.svg';
        else
          svgAssetPath+='clear_sky_night.svg';
        break;
      case WeatherStatusEum.Clouds:
        if(weatherStatusDescription.contains('few clouds')){
          if(icon.contains('d'))
            svgAssetPath+='few_clouds_day.svg';
          else
            svgAssetPath+='few_clouds_night.svg';
        }
        else if(weatherStatusDescription.contains('scattered clouds'))
          svgAssetPath+='acattered_clouds.svg';
        else
          svgAssetPath+='broken_clouds.svg';
        break;
      case WeatherStatusEum.UnKnown:
        svgAssetPath+='clear_sky_day.svg';
        break;
    }
  }



}

enum WeatherStatusEum{
  Thunderstorm,Drizzle,Rain,Snow,Mist,Smoke,Haze,Dust,Fog,Sand,Ash,Squall,Tornado,Clear,Clouds,UnKnown
}