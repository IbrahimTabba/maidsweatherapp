class CityModel{
  final String name;
  final DateTime sunrise;
  final DateTime sunset;

  CityModel(this.name , this.sunrise , this.sunset);

  CityModel.fromJson(obj):
      name = obj['name'],
      sunrise = DateTime.fromMillisecondsSinceEpoch(obj['sunrise']*1000).toUtc().add(Duration(milliseconds: obj['timezone']*1000)),
      sunset = DateTime.fromMillisecondsSinceEpoch(obj['sunset']*1000).toUtc().add(Duration(milliseconds: obj['timezone']*1000));


  bool get day{
    var now = DateTime.now().toUtc();
    return now.isAfter(sunrise) && now.isBefore(sunset);
  }

  bool get night => !day;


}