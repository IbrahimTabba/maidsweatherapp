import 'package:maidsweatherapp/models/city_model.dart';
import 'package:maidsweatherapp/models/day_weather_forecast_model.dart';

class WeatherForeCastResponseModel{
  final List<DayWeatherForeCastModel> daysForecast;
  final int daysCount;
  final CityModel city;
  final int code;
  final String message;
  final bool error;

  WeatherForeCastResponseModel({this.daysForecast, this.daysCount, this.code, this.city, this.message, this.error});

  WeatherForeCastResponseModel.fromJson(Map<String,dynamic> obj):
      daysForecast = obj['list']!=null?
          (obj['list'] as List).map((forecast) => DayWeatherForeCastModel.fromJson(forecast)).toList():
          <DayWeatherForeCastModel>[],
      daysCount = obj['cnt'],
      city = obj['city']!=null?CityModel.fromJson(obj['city']):null,
      code = int.parse(obj['cod']),
      message = "${obj['message']}",
      error = obj['cod']!="200";

  WeatherForeCastResponseModel.withError({code}):
      daysForecast=<DayWeatherForeCastModel>[],
      daysCount=0,
      city=null,
      code = code,
      message = '',
      error = true;

  List<DayWeatherForeCastModel> getByDate(DateTime date){
    var groups = <String,List<DayWeatherForeCastModel>>{};
    var result = <DayWeatherForeCastModel>[];
    //Group by day
    daysForecast.forEach((element) {
      var key = "${element.foreCastedDate.year}-${element.foreCastedDate.month}-${element.foreCastedDate.day}";
      if(!groups.containsKey(key))
        groups[key] = <DayWeatherForeCastModel>[];
      groups[key].add(element);
    });

    //Get the closest forecast for the required day
    print(groups.length);
    groups.forEach((key, value) {
      var found = false;
      for(int i = 0 ; i < value.length ; i++){
        if(value[i].foreCastedDate.isAfter(date)){
          found = true;
          result.add(value[i]);
          break;
        }
      }
      if(!found)
        result.add(value.last);
      date = date.add(Duration(days: 1));
    });
    return result.take(3).toList();
  }

}